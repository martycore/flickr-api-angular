angular.module('FlickrApp', [
  'ngRoute'
]).config(function ( $routeProvider ) {
  'use strict';
  $routeProvider
    .when('/feed', {
      templateUrl: 'views/feed.html',
      controller: 'FeedCtrl'
    })
    .otherwise({
      redirectTo: '/feed'
    });
});
