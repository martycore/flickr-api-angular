/**
 * Controller: FeedCtrl
 */
angular.module('FlickrApp')
  .controller('FeedCtrl',
    function FeedCtrl ( $scope, FeedFactory ) {
      'use strict';
      $scope.meta = {
        title: "Flickr Photo Stream"
      };
      FeedFactory.getMessages()
        .success(function(jsonData, statusCode){
            console.log('The request was successful!', statusCode);
            console.dir(jsonData);
            // Now add the photo stream to the controller's scope
            $scope.data = {
              photos: jsonData
            };
        });
    });